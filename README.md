# gff2pandas

Simple GFF3 parser in python pandas.

## Usage

`dfChr = parseGFF('ensembl/Homo_sapiens.GRCh38.93.chromosome.1.gff3')`

(with data from Ensembl: ftp://ftp.ensembl.org/pub/release-93/gff3/homo_sapiens/ )

## Todo

- [ ] In progress (branch `subclassingPandasDataFrame`): create a class `DataFrameGFF` that inherits from `pandas.DataFrame`? (cf. [pandas documentation on extending and subclassing pandas](http://pandas.pydata.org/pandas-docs/stable/extending.html#extending-subclassing-pandas) )

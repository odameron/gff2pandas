#! /usr/bin/env python3

import pandas as pd

gff3columns = ['seqid', 'source', 'type', 'start', 'end', 'score', 'strand', 'phase', 'attributes']
gff3attributes = ['ID', 'Name', 'Alias', 'Parent', 'Dbxref', 'Ontology_term', 'Is_circular']

# TODO: not necessary?
def parseGFFattributesField(attributesContent):
    attrb = {}
    for currentAttr in attributesContent.split(';'):
        (attrbName, attrbValue) = currentAttr.split('=', 1)
        attrb[attrbName] = attrbValue
    return attrb

def getGFFattributeValue(attributesContent, attributeName):
    if pd.isnull(attributesContent):
        return attributesContent
    for currentAttr in str(attributesContent).split(';'):
        (attrbName, attrbValue) = currentAttr.split('=', 1)
        if attrbName == attributeName:
            return attrbValue
    return ""

def getGFFattributeNames(attributesContent):
    """
    returns the list of the attributes' names in a particular attributes field value.
    """
    attrNames = []
    for currentAttr in attributesContent.split(';'):
        attrNames.append(currentAttr.split('=', 1)[0])
    return attrNames

def getDataFrameAttributesNames(dfGFF):
    """
    returns the set of the attributes' names in at least one of the attributes field of the DataFrame generated from a GFF3 file.
    """
    attributes = set()
    for x in dfGFF['attributes']:
        attributes.update(getGFFattributeNames(x))
    return attributes

def expandAttributesInGFFDataFrame(dfGFF):
    """
    adds new columns to a DataFrame generated from a GFF3 file according to the pairs in the attributes field.
    """
    dfs = [dfGFF]
    for currentAttributeName in getDataFrameAttributesNames(dfGFF):
        #print("\n" + currentAttributeName)
        dfs.append(pd.DataFrame([getGFFattributeValue(x, currentAttributeName) for x in dfGFF['attributes']], columns=[currentAttributeName], index=dfGFF.index))
    return pd.concat(dfs, axis=1)

def parseGFF(gffFilePath, expandAttributesField=True):
    """
    Parses a GFF3 file and returns the resul as a pandas DataFrame.
    """
    #dfGFF = pd.read_csv(gffFilePath, sep='\t', skiprows=7, comment='#', na_values='.', names=gff3columns)
    #dfGFF = pd.read_csv(gffFilePath, sep='\t', comment='#', na_values='.', names=gff3columns)
    #dfGFF = pd.read_csv(gffFilePath, sep='\t', comment='#', skip_blank_lines=True, na_values='.', names=gff3columns)
    dfGFF = pd.read_csv(gffFilePath, sep='\t', comment='#', skip_blank_lines=True, na_values='.', names=gff3columns, low_memory=False)	# set low_memory to False as the default may be a problem with somme GFF (e.g. EBI Homo_sapiens.GRCh38.93.chr.gff3 ; solution from https://stackoverflow.com/questions/25488675/mixed-types-when-reading-csv-files-causes-fixes-and-consequences/25488801
	if expandAttributesField:
		return dfGFF
	else:
		return expandAttributesInGFFDataFrame(dfGFF)


